import type { ReceiptItem } from "./ReceiptItem"
import type { User } from "./User"
import type { Member } from "./Member"

type Receipt = {
    id: number
    createdDate: Date
    totalBefore: number
    total: number
    ReceiptAmount: number
    change: number
    payType: string
    userId: number
    user?: User
    memberId: number
    member?: Member
    memberDiscount: number
    receiptItems?: ReceiptItem[]
  }

export {type Receipt}