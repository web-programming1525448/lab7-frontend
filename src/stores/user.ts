import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/userService'
import type { User } from '@/types/User'
import { useLoadingStore } from './loading'

export const useUserStore = defineStore('user', () => {
  const users = ref<User[]>([])
  const loadingStore = useLoadingStore()
  const initialdUser: User = {
    email: '',
    password: '',
    fullName: '',
    gender: 'male',
    roles: ['user']
  }

  const editedUser = ref<User>(JSON.parse(JSON.stringify(initialdUser)))

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    editedUser.value = res.data
    loadingStore.finish()
  }

  async function saveUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    if (!user.id) {
      console.log('Add New User ')
      console.log(editedUser.value)
      const res = await userService.addUser(user)
    } else {
      console.log('Update User Id: ' + user.id)
      const res = await userService.update(user)
    }
    await getUsers()
    loadingStore.finish()
  }

  async function delUser() {
    loadingStore.doLoad()
    const user = editedUser.value
    const res = await userService.delUser(user)
    await getUsers()
    loadingStore.finish()
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialdUser))
  }

  return { users, editedUser, getUsers, saveUser, delUser, getUser, clearForm }
})
