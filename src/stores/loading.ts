import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('Loading', () => {
  const loading = ref(false)
  const doLoad = () => {
    loading.value = true
  }
  const finish = () => {
    loading.value = false
  }
  return { loading, doLoad, finish }
})
