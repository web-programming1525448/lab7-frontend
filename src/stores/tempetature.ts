import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import temperatureService from '../services/temperatureService'
import { useLoadingStore } from './loading'

export const useTemperatureStore = defineStore('temperature', () => {
  const valid = ref(false)
  const celsius = ref(0)
  const result = ref(0)
  const loadingStore = useLoadingStore()

  async function callConvert() {
    console.log('call convert')
    // result.value = convert(celsius.value)
    loadingStore.doLoad()
    try {
      result.value = await temperatureService.convert(celsius.value)
    } catch (error) {
      console.log(error)
    }
    loadingStore.finish()
    console.log('finish call convert')
  }

  return {celsius, result, valid, callConvert}
})
